package co.velloso.android.extensions

fun Boolean.toStringYesNo(
        yesString: String,
        noString: String
): String = if (this) yesString else noString