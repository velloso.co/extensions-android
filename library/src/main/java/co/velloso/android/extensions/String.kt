package co.velloso.android.extensions

import android.annotation.SuppressLint
import android.text.SpannableStringBuilder
import java.io.IOException
import java.security.NoSuchAlgorithmException
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*


@SuppressLint("SimpleDateFormat")
fun String.toDate(pattern: String = "yyyyMMddHHmmss", locale: String = "GMT"): Date {
    val simpleDateFormat = SimpleDateFormat(pattern)
    simpleDateFormat.timeZone = TimeZone.getTimeZone(locale)
    return simpleDateFormat.parse(this)!!
}

fun String.toBarcodeString(): String {
    val builder = StringBuilder(this.length + this.length / 4 + 1)
    var index = 0
    var prefix = ""
    while (index < this.length) {
        // Don't put the upsert in the very first iteration.
        // This is easier than appending it *after* each substring
        builder.append(prefix)
        prefix = " "
        builder.append(this.substring(index, Math.min(index + 4, this.length)))
        index += 4
    }
    return builder.toString()
}

@Throws(IOException::class)
fun String.compress(): ByteArray = this.toByteArray().compress()

fun String.chunked(size: Int): Array<String> {
    val remainder = length % size
    val nChunks = length / size
    val array = Array(if (remainder > 0) nChunks + 1 else nChunks) { "" }
    (0 until nChunks).forEach {
        array[it] = substring(it * size, (it + 1) * size)
    }
    if (remainder > 0) array[nChunks] = substring(nChunks * size, (nChunks * size) + remainder)
    return array
}


/**
 * Cryptography string to MD5
 */
fun String.md5(): String {
    try {
        // Create MD5 Hash
        val digest = java.security.MessageDigest.getInstance("MD5")
        digest.update(this.toByteArray())
        val messageDigest = digest.digest()

        // Create Hex String
        val hexString = StringBuilder()
        for (aMessageDigest in messageDigest) {
            var h = Integer.toHexString(0xFF and aMessageDigest.toInt())
            while (h.length < 2)
                h = "0$h"
            hexString.append(h)
        }
        return hexString.toString()

    } catch (e: NoSuchAlgorithmException) {
        e.printStackTrace()
    }
    return ""
}


fun String.crc16(polynomial: Int, initialValue: Int, finalXorValue: Int): Int =
        this.toByteArray().crc16(polynomial, initialValue, finalXorValue)


fun String.hexStringToByteArray(): ByteArray {
    val b = ByteArray(this.length / 3)
    b.forEachIndexed { index, _ ->
        val position = index * 3
        val value = Integer.parseInt(this.substring(position, position + 2), 16)
        b[index] = value.toByte()
    }
    return b
}

fun String.removeAccent() = StringUtil.removeAccent(this)!!

fun String.macAddressToCompositeId(): Pair<Int, Int> {
    val vendorId = Regex("[^0-9]").replace(this.substring(0, 9), "").toInt()
    val productId = Regex("[^0-9]").replace(this.substring(9), "").toInt()
    return Pair(vendorId, productId)
}

fun String.onlyNumeric(): String = this.replace("[^\\d]".toRegex(), "")


fun String.countOccurrences(occurrence: String): Int =
        this.length - this.replace(occurrence, "").length

fun String.isNumeric(): Boolean = try {
    java.lang.Double.parseDouble(this)
    true
} catch (nfe: NumberFormatException) {
    false
}

fun String.toDoubleLocale(): Double? = NumberFormat.getNumberInstance().parse(this)?.toDouble()

/**
 *
 */
fun String.formatCep(): String = SpannableStringBuilder(this).formatCep().toString()

fun String.formatCreditCard(): String = SpannableStringBuilder(this).formatCreditCard().toString()

fun String.formatValidThrough(): String =
        SpannableStringBuilder(this).formatValidThrough().toString()

fun String.cleanCpf(): String = this.replace("[^\\d]".toRegex(), "")

fun String.cleanCnpj(): String = this.replace("[^\\d]".toRegex(), "")

fun String.cleanPhone(): String = this.replace("[^\\d]".toRegex(), "")

fun String.cleanZipCode(): String = this.replace("[^\\d]".toRegex(), "")

fun String.formatTaxPayerId(): String = SpannableStringBuilder(this).formatTaxPayerId().toString()

fun String.formatCpf(): String = SpannableStringBuilder(this).formatCpf().toString()

fun String.formatCnpj(): String = SpannableStringBuilder(this).formatCnpj().toString()