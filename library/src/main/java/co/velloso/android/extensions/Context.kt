package co.velloso.android.extensions

import android.accounts.AccountManager
import android.app.AlarmManager
import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.util.TypedValue
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat

/**
 * Android resources extensions
 */
fun Context.getAttributeDimen(attr: Int): Int? {
    val tv = TypedValue()
    return if (theme.resolveAttribute(attr, tv, true)) {
        TypedValue.complexToDimensionPixelSize(tv.data, resources.displayMetrics)
    } else null
}

fun Context.getDpDimen(dp: Float): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics).toInt()
}

inline fun <R> Context.useAttributes(attrs: AttributeSet?, typedArrayIds: IntArray, block: (TypedArray) -> R): R {
    val typedArray = resources.obtainAttributes(attrs, typedArrayIds)
    return block(typedArray).also {
        typedArray.recycle()
    }
}

fun Context.getDrawableCompat(@DrawableRes id: Int) = ContextCompat.getDrawable(this, id)

fun Context.getColorCompat(@ColorRes id: Int) = ContextCompat.getColor(this, id)

fun Context.getAlarmManager() = getSystemService(Context.ALARM_SERVICE) as AlarmManager

fun Context.getNotificationManager() = NotificationManagerCompat.from(this)

fun Context.getAccountManager() = AccountManager.get(this)