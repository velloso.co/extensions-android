package co.velloso.android.extensions;

import java.text.Normalizer;

public class StringUtil {

    public static String removeAccent(String string) {
        return Normalizer
                .normalize(string, Normalizer.Form.NFD)
                .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }
}
