package co.velloso.android.extensions

import android.graphics.drawable.Drawable
import androidx.annotation.ColorInt
import androidx.core.graphics.drawable.DrawableCompat

fun Drawable.setTintCompat(@ColorInt tint: Int) = DrawableCompat.setTint(this.mutate(), tint)