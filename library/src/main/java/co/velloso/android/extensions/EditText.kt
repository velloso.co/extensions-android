package co.velloso.android.extensions

import android.widget.EditText
import androidx.annotation.StringRes


fun EditText.isValid(regex: Regex,
                     @StringRes failureMessage: Int? = null,
                     success: ((value: String) -> Unit)? = null
): Boolean {
    return if (text.matches(regex)) {
        success?.invoke(text.toString())
        true
    } else {
        failureMessage?.let {
            this.error = this.context.getString(failureMessage)
        }
        false
    }
}