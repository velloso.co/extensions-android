package co.velloso.android.extensions

import java.util.*

fun Long.toDate() = Date(this)
