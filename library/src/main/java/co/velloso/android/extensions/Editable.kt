package co.velloso.android.extensions

import android.text.Editable


fun Editable.formatTaxPayerId(): Editable {
    when {
        this.getOrNull(2)?.toString() == "." -> this.formatCnpj()
        this.toString().onlyNumeric().length > 11 -> this.removeNonNumeric().formatCnpj()
        else -> this.formatCpf()
    }
    return this
}

fun Editable.toDoubleLocale() = this.toString().toDoubleLocale()

fun Editable.removeNonNumeric(): Editable {
    this.forEachIndexed { i, char ->
        if (!char.toString().isNumeric()) this.delete(i, i + 1)
    }
    return this
}

fun Editable.formatCpf(): Editable {
    if (this.toString().countOccurrences(".") < 1 && this.length > 3) {
        this.insert(3, ".")
    }
    if (this.toString().countOccurrences(".") < 2 && this.length > 7) {
        this.insert(7, ".")
    }
    if (!this.toString().contains("-") && this.length > 11) {
        this.insert(11, "-")
    }
    return this
}

fun Editable.formatCnpj(): Editable {
    if (this.toString().countOccurrences(".") < 1 && this.length > 2) {
        this.insert(2, ".")
    }
    if (this.toString().countOccurrences(".") < 2 && this.length > 6) {
        this.insert(6, ".")
    }
    if (this.toString().countOccurrences("/") < 1 && this.length > 10) {
        this.insert(10, "/")
    }
    if (!this.toString().contains("-") && this.length > 15) {
        this.insert(15, "-")
    }
    return this
}

fun Editable.formatCep(): Editable {
    if (!toString().contains("-") && length > 5) {
        insert(5, "-")
    }
    return this
}

fun Editable.formatCreditCard(): Editable {
    if (this.toString().countOccurrences(" ") < 1 && this.length > 4) {
        this.insert(4, " ")
    }
    if (this.toString().countOccurrences(" ") < 2 && this.length > 9) {
        this.insert(9, " ")
    }
    if (this.toString().countOccurrences(" ") < 3 && this.length > 14) {
        this.insert(14, " ")
    }
    return this
}

fun Editable.formatValidThrough(): Editable {
    if (this.toString().countOccurrences("/") < 1 && this.length > 2) {
        this.insert(2, "/")
    }
    return this
}