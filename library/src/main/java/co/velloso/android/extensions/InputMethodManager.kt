package co.velloso.android.extensions

import android.view.View
import android.view.inputmethod.InputMethodManager


fun InputMethodManager.show() = this.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)

fun InputMethodManager.hide(view: View) = this.hideSoftInputFromWindow(view.windowToken, 0)
