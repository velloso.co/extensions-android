package co.velloso.android.extensions

import android.content.res.Resources
import java.text.NumberFormat
import java.util.*

fun Double.toStringPriceDecimal(): String {
    val integer = this.toInt()
    val fractional = this - integer
    return fractional.toStringPrice().substring(1)
}

fun Double.toStringPrice(currencySymbol: String?, showDecimal: Boolean = true, prepend: Boolean = true): String {
    val numberFormatter = NumberFormat.getNumberInstance()
    numberFormatter.minimumFractionDigits = if (showDecimal) 2 else 0
    return if (currencySymbol != null) {
        if (prepend) "$currencySymbol ${numberFormatter.format(this)}"
        else "${numberFormatter.format(this)} $currencySymbol"
    } else {
        String.format(numberFormatter.format(this))
    }
}

fun Double.toStringPrice(): String = this.toStringPrice(null)

fun Double.toString(template: String): String = String.format(template, this)

fun Double.toString(resources: Resources, resourceId: Int): String =
        String.format(resources.getString(resourceId))

fun Double.toStringQuantity(digits: Int): String = String.format(Locale.US, "%.${digits}f", this)

fun Double.toStringMinimal(): String {
    return if (this == this.toLong().toDouble()) String.format("%d", this.toLong())
    else String.format("%s", this)
}
