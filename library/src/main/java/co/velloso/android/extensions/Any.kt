package co.velloso.android.extensions


fun <T : Any> checkDiff(value: T?, secondValue: T?, lazyMessage: () -> Any): T {
    if (value == null || value == secondValue) throw RuntimeException(lazyMessage.toString())
    return value
}

