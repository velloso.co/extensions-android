package co.velloso.android.extensions

import android.content.Context
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment

fun Fragment.inputMethodManager() = lazy {
    requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
}