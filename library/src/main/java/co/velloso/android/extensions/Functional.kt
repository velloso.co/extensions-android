package co.velloso.android.extensions

/**
 * Functional extensions
 */
inline fun <T> Iterable<T>.findIndex(predicate: (T) -> Boolean): Int? {
    return indexOf(firstOrNull(predicate) ?: return null)
}

fun <K, V> Iterable<Map.Entry<K, V>>.findKey(value: V): K? {
    forEach {
        if (it.value == value) return it.key
    }
    return null
}

