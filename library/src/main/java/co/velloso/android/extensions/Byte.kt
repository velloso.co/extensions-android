package co.velloso.android.extensions

import kotlin.experimental.and

fun Byte.toBitString() = String.format("%8s", Integer.toBinaryString((this and 0xFF.toByte()).toInt())).replace(' ', '0')
