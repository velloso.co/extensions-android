package co.velloso.android.extensions

import android.annotation.SuppressLint
import android.text.format.DateUtils
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


/**
 * Date extensions
 */
fun Date.daysFromToday() = ((Date().time - this.time) / (1000 * 60 * 60 * 24)).toInt()

fun Date.toSimpleDateString(todayString: String, yesterdayString: String, standardFormat: DateFormat): String = when {
    DateUtils.isToday(this.time) -> todayString
    DateUtils.isToday(this.time.plus(DateUtils.DAY_IN_MILLIS)) -> yesterdayString
    else -> standardFormat.format(this)
}

@SuppressLint("SimpleDateFormat")
fun Date.toSimpleDateTimeString(
        todayString: String,
        yesterdayString: String,
        standardFormat: DateFormat,
        timeFormat: DateFormat

): String = when {
    DateUtils.isToday(this.time) -> "$todayString ${timeFormat.format(this)}"
    DateUtils.isToday(this.time.plus(DateUtils.DAY_IN_MILLIS)) -> "$yesterdayString ${timeFormat.format(this)}"
    else -> standardFormat.format(this)
}

fun Date.toSimpleTimeString(timeFormat: DateFormat): String = timeFormat.format(this)

/**
 * @param template the template that concatenate date and time such as {@code $1%s at $2%s}
 * @param todayString the word that represent today in the application language
 * @param yesterdayString the word that represent yesterday in the application language
 * @param dateFormat the DateFormat that should be used for the case it's not today or yesterday
 * @param timeFormat the DateFormat that should be used for time part of string
 *
 * @return a simple datetime String such as {@code Yesterday at 13:00} or
 * {@code Jan 1, 2017 at 14:34}.
 */
fun Date.toSimpleDateTimeString(
        template: String,
        todayString: String,
        yesterdayString: String,
        dateFormat: DateFormat,
        timeFormat: DateFormat
): String = template.format(this.toSimpleDateString(todayString, yesterdayString, dateFormat), this.toSimpleTimeString(timeFormat))

@SuppressLint("SimpleDateFormat")
fun Date.toDateString(pattern: String = "yyyyMMddHHmmss", timeZone: TimeZone? = null): String {
    val simpleDateFormat = SimpleDateFormat(pattern)
    simpleDateFormat.timeZone = timeZone ?: TimeZone.getDefault()
    return simpleDateFormat.format(this)
}