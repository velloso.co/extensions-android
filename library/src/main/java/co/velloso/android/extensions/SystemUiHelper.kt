package co.velloso.android.extensions

import android.os.Handler
import android.view.View
import android.view.Window
import android.view.WindowManager


class SystemUiHelper(
        private val window: Window,
        private val container: View,
        private val handler: Handler = Handler()
) {

    private val originalSystemUiVisibility = window.decorView.systemUiVisibility

    private val hiddenNotificationVisibility = originalSystemUiVisibility or
            View.SYSTEM_UI_FLAG_LOW_PROFILE

    private val hiddenSystemUiVisibility = View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION

    private val mHiddenNotificationRunnable = Runnable {
        window.decorView.systemUiVisibility = hiddenNotificationVisibility
    }

    private val mHiddenSystemUiVisibility = Runnable {
        window.decorView.systemUiVisibility = hiddenSystemUiVisibility
    }

    private fun clearAllFlags() {
        window.clearFlags(hiddenSystemUiVisibility or
                WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN or
                WindowManager.LayoutParams.FLAG_FULLSCREEN or
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        window.decorView.systemUiVisibility = originalSystemUiVisibility
    }

    fun showSystemUi() {
        // Handler
        handler.removeCallbacks(mHiddenNotificationRunnable, mHiddenSystemUiVisibility)

        // Container
        container.fitsSystemWindows = false

        // Flags
        window.addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
        clearAllFlags()

        // DecorView
        window.decorView.setOnSystemUiVisibilityChangeListener(null)
        window.decorView.systemUiVisibility = originalSystemUiVisibility
        window.decorView.requestLayout()
    }

    fun hideNotification() {
        // Handler
        handler.removeCallbacks(mHiddenNotificationRunnable, mHiddenSystemUiVisibility)

        // Container
        container.fitsSystemWindows = false

        // Flags
        clearAllFlags()

        // DecorView
        window.decorView.setOnSystemUiVisibilityChangeListener(null)
        window.decorView.systemUiVisibility = hiddenNotificationVisibility
        window.decorView.setOnSystemUiVisibilityChangeListener {
            if (window.decorView.systemUiVisibility != hiddenNotificationVisibility) {
                handler.postDelayed(mHiddenNotificationRunnable, 3000)
            }
        }
        window.decorView.requestLayout()
    }

    fun hideStatusBar() {
        // Handler
        handler.removeCallbacks(mHiddenNotificationRunnable, mHiddenSystemUiVisibility)

        // Container
        container.fitsSystemWindows = false

        // Flags
        clearAllFlags()
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        // DecorView
        window.decorView.setOnSystemUiVisibilityChangeListener(null)
        window.decorView.systemUiVisibility = originalSystemUiVisibility
        window.decorView.requestLayout()
    }

    fun hideSystemUi() {
        // Handler
        handler.removeCallbacks(mHiddenNotificationRunnable, mHiddenSystemUiVisibility)

        // Container
        container.fitsSystemWindows = false

        // Flags
        clearAllFlags()
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        // DecorView
        window.decorView.setOnSystemUiVisibilityChangeListener(null)
        window.decorView.systemUiVisibility = hiddenSystemUiVisibility
        window.decorView.setOnSystemUiVisibilityChangeListener { _ ->
            if (window.decorView.systemUiVisibility != hiddenSystemUiVisibility) {
                handler.postDelayed(mHiddenSystemUiVisibility, 700)
            }
        }
        window.decorView.requestLayout()
    }
}