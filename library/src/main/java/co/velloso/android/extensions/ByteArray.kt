package co.velloso.android.extensions

import android.os.Environment
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.nio.ByteBuffer
import java.util.*
import java.util.zip.DataFormatException
import java.util.zip.Deflater
import java.util.zip.Inflater


/**
 * converts the given String to CRC16
 *
 * @param polynomial
 * - the polynomial (divisor)
 * @param initialValue
 * - the initial CRC value
 * @param finalXorValue
 * - the value that the crc should be applied with a xor bitwise operation
 * ASCII
 * @return
 */
fun ByteArray.crc16(polynomial: Int, initialValue: Int, finalXorValue: Int): Int {
    var crc = initialValue

    val intArray = IntArray(this.size)
    for ((ctr, b) in this.withIndex()) {
        intArray[ctr] = b.toInt()
    }

    // Main code for computing the 16-bit CRC-CCITT
    intArray.forEach {
        for (i in 0..7) {
            val bit = it shr 7 - i and 1 == 1
            val c15 = crc shr 15 and 1 == 1
            crc = crc shl 1
            if (c15 xor bit) crc = crc xor polynomial
        }
    }

    return crc and finalXorValue
}

@Throws(IOException::class, DataFormatException::class)
fun ByteArray.uncompress(bufferSize: Int): ByteArray {
    // Decompress the bytes
    val inflater = Inflater()
    inflater.setInput(this)
    val buffer = ByteArray(bufferSize)
    inflater.inflate(buffer)
    inflater.end()
    return buffer
}

fun ByteArray.checkSum() = this.sum() % 256

@Throws(IOException::class)
fun ByteArray.compress(): ByteArray {
    val deflater = Deflater()
    deflater.setInput(this)
    val outputStream = ByteArrayOutputStream(this.size)
    deflater.finish()
    val buffer = ByteArray(1024)
    while (!deflater.finished()) {
        val count = deflater.deflate(buffer) // returns the generated code... index
        outputStream.write(buffer, 0, count)
    }
    outputStream.close()
    return outputStream.toByteArray()
}

fun ByteArray.split(delimiter: ByteArray): List<ByteArray> {
    val byteArrays = LinkedList<ByteArray>()
    if (delimiter.isEmpty()) return byteArrays.toList()

    var begin = 0
    outer@ for (i in 0 until this.size - delimiter.size + 1) {
        for (j in delimiter.indices) {
            if (this[i + j] != delimiter[j]) {
                continue@outer
            }
        }
        byteArrays.add(Arrays.copyOfRange(this, begin, i))
        begin = i + delimiter.size
    }
    byteArrays.add(Arrays.copyOfRange(this, begin, this.size))
    return byteArrays
}

fun ByteArray.split(delimiter: Byte) = this.split(kotlin.byteArrayOf(delimiter))

fun ByteArray.removeLast(last: Int = 1): ByteArray {
    return Arrays.copyOfRange(this, 0, this.size - last)
}

fun ByteArray.prepend(byte: Byte): ByteArray {
    return ByteBuffer.allocate(this.size + 1).put(byte).put(this).array()
}

fun ByteArray.append(byte: Byte): ByteArray {
    return ByteBuffer.allocate(this.size + 1).put(this).put(byte).array()
}

fun ByteArray.append(vararg elements: Byte): ByteArray {
    val byteArray = byteArrayOf(*elements)
    val c = ByteArray(this.size + byteArray.size)
    System.arraycopy(this, 0, c, 0, this.size)
    System.arraycopy(byteArray, 0, c, this.size, byteArray.size)
    return c
}

fun ByteArray.toHexString(): String {
    var s = ""
    this.forEach { s = s + String.format("%02X", it) + " " }
    return s
}


@Throws(IOException::class)
fun ByteArray.saveFile(path: String, filename: String) {
    val root = Environment.getExternalStorageDirectory().toString()
    val directory = File(root + path)
    directory.mkdirs()
    val file = File(directory, filename)
    val out = FileOutputStream(file)
    out.write(this)
    out.flush()
    out.close()
}