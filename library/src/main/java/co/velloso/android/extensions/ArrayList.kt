package co.velloso.android.extensions

import java.util.*


fun <T> ArrayList<T>.prepend(item: T): ArrayList<T> {
    this.add(0, item)
    return this
}

fun <T> ArrayList<T>.append(item: T): ArrayList<T> {
    this.add(item)
    return this
}