package co.velloso.android.extensions

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.FrameLayout
import androidx.core.view.ViewCompat

/**
 * Android Views extensions
 */
fun View.setSize(width: Int? = null, height: Int? = null) {
    val params = layoutParams
    if (width != null) params.width = width
    if (height != null) params.height = height
    layoutParams = params
}

fun View.setMarginInFrameLayout(left: Int, top: Int, right: Int, bottom: Int) {
    val marginParams = ViewGroup.MarginLayoutParams(layoutParams)
    marginParams.setMargins(left, top, right, bottom)
    // We use FrameLayout.LayoutParams because the view is inside a FrameLayout
    layoutParams = FrameLayout.LayoutParams(marginParams)
}

fun View.setPaddingDimen(dimenLeft: Int? = null, dimenTop: Int? = null, dimenRight: Int? = null, dimenBottom: Int? = null) {
    val left = if (dimenLeft != null) context.resources.getDimensionPixelSize(dimenLeft) else 0
    val top = if (dimenTop != null) context.resources.getDimensionPixelSize(dimenTop) else 0
    val right = if (dimenRight != null) context.resources.getDimensionPixelSize(dimenRight) else 0
    val bottom = if (dimenBottom != null) context.resources.getDimensionPixelSize(dimenBottom) else 0
    setPadding(left, top, right, bottom)
}

fun View.setAutofillHintsCompat(vararg hints: String) = ViewCompat.setAutofillHints(this, *hints)

fun EditText.focusOn() {
    requestFocus()
    (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).show()
}

fun EditText.focusOff() {
    clearFocus()
    (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hide(this)
}